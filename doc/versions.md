Versionning des vocabulaires et de l'ontologie DDF
=====================================================


# Requêter les versions

## Vocabulaires

```sparql

SELECT *
WHERE {
    ?voc a skos:ConceptScheme  ;
    dct:modified ?dateModifiedTest .
    SERVICE <repository:DDF> {
        ?voc dct:modified ?dateModifiedProd .
    }
    BIND (
      COALESCE(
        IF(xsd:dateTime(?dateModifiedTest) >= xsd:dateTime(?dateModifiedProd), "Test Has Latestest Version", ""),
        IF(xsd:dateTime(?dateModifiedTest) < xsd:dateTime(?dateModifiedProd), "Prod has latest version ", ""),
        "Not comparable"
      ) AS ?VersionComparison
)
}



```