
$files=(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA);

for i in $files; do set -- $i ; echo $1 ; mkdir -p public/voc/$1 ;  ontospy gendocs -o public/voc/$1 --type 2 --theme cosmo --nobrowser vocabularies/$1.ttl || true ; ls -al ; done

pwdgit git 